#!/usr/bin/env python

''' dpl-utils setup script '''

from distutils         import log
from distutils.core    import setup
from distutils.cmd     import Command
from distutils.command import install_scripts

from glob import glob
from stat import ST_MODE

from subprocess import check_call

import os
import sys

# Script installer

class InstallScripts(install_scripts.install_scripts):
    def run(self):
        if not self.skip_build:
            self.run_command('build_scripts')
        self.outfiles = self.copy_tree(self.build_dir, self.install_dir)
        if os.name == 'posix':
            for file in self.get_outputs():
                if self.dry_run:
                    log.info("changing mode of %s", file)
                else:
                    mode = ((os.stat(file)[ST_MODE]) | 0o555) & 0o7777
                    log.info("changing mode of %s to %o", file, mode)
                    os.chmod(file, mode)
                    # Basically the same but remove .py extension
                    file_new = '.'.join(file.split('.')[:-1])
                    os.rename(file, file_new)
                    log.info("renaming %s to %s", file, file_new)


# Setup Configuration

setup(
    name         = 'dpl-utils',
    version      = '20140327',
    description  = 'Collection of utilities for DPL',
    author       = 'Peter Bui',
    author_email = 'buipj@uwec.edu',
    url          = 'http://bitbucket.org/pbui/dpl-utils',
    packages     = ['dpl',
                    'dpl.catalog',
                    'dpl.catalog.plugins',
                    'dpl.catalog.templates',
                   ],
    package_dir  = {'dpl.catalog.templates': 'dpl/catalog/templates'},
    package_data = {'dpl.catalog.templates': ['*.tmpl']},
    scripts      = ['bin/catalog.py'],
    cmdclass     = {'install_scripts': InstallScripts}
)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
