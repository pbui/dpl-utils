#!/usr/bin/env python3

''' Catalog '''

import os
import shlex
import sys

import baker

sys.path.insert(0, os.curdir)
sys.path.insert(0, os.path.dirname(os.path.dirname(sys.argv[0])))

import dpl.catalog.plugins
import dpl.util

from dpl.catalog.client import CatalogClient
from dpl.catalog.server import CatalogServer

# Catalog server command

@baker.command(shortopts={'port': 'p', 'title': 't'})
def server(port=0, title=None, *settings):
    ''' Start Catalog Server

    :param port:        Catalog port
    :param title:       Catalog title
    :param settings:    Additional catalog settings
    '''
    settings = dpl.util.parse_option_pairs(settings)
    catalog_server = CatalogServer(port, title, **settings)
    catalog_server.run()

# Catalog ssh/sftp command

def _ssh(command, ssh_host, catalog=None, ssh_options=None):
    hostname, port = dpl.util.parse_host(catalog)
    catalog_client = CatalogClient(hostname, port)
    services       = catalog_client.query().get('services', {})

    if ssh_options is None:
        ssh_options = ''

    for service in services:
        if ssh_host == service.get('name', None):
            os.execvp(command, [command, service.get('address', None) ] + shlex.split(ssh_options))

    print('Unknown host:', ssh_host, file=sys.stderr)
    sys.exit(1)

@baker.command(shortopts={'catalog': 'c', 'ssh_options': 'o'})
def ssh(ssh_host, catalog=None, ssh_options=None):
    ''' SSH to host in Catalog

    :param catalog:     Catalog host (hostname:port)
    :param ssh_host:    SSH host
    :param ssh_options: SSH arguments
    '''
    _ssh('ssh', ssh_host, catalog, ssh_options)


@baker.command(shortopts={'catalog': 'c', 'ssh_options': 'o'})
def sftp(ssh_host, catalog=None, ssh_options=None):
    ''' SFTP to host in Catalog

    :param catalog:     Catalog host (hostname:port)
    :param ssh_host:    SSH host
    :param ssh_options: SSH arguments
    '''
    _ssh('sftp', ssh_host, catalog, ssh_options)

# Catalog status command

@baker.command(shortopts={'catalog': 'c'})
def status(catalog=None):
    ''' Display services in Catalog

    :param catalog:    Catalog host (hostname:port)
    '''
    hostname, port = dpl.util.parse_host(catalog)
    catalog_client = CatalogClient(hostname, port)
    catalog_client.status()

# Catalog update command

@baker.command(shortopts={'catalog': 'c'})
def update(catalog=None, *properties):
    ''' Send Catalog an update message

    Command line arguments in the form key=value will be interpreted as
    properties to be appended to the update message.

    :param catalog:     Catalog host (hostname:port)
    :param properties:  List of name=value properties
    '''
    # Add properties to update record
    record = dpl.util.parse_option_pairs(properties)

    # Process plugins and add to update record
    plugins = ['services', 'users', 'cpu', 'disk', 'memory', 'processes', 'torque']
    record.update(dpl.catalog.plugins.load_plugins(plugins))

    # Send update record
    hostname, port = dpl.util.parse_host(catalog)
    catalog_client = CatalogClient(hostname, port)
    catalog_client.update(**record)

# Dispatch catalog commands

if __name__ == '__main__':
    baker.run()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
