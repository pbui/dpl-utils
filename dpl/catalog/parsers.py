''' DPL Catalog - Parsers '''

import json
import yaml


class JSON(object):
    @staticmethod
    def dumps(data):
        return json.dumps(data)

    @staticmethod
    def loads(text):
        return json.loads(text)

class YAML(object):
    @staticmethod
    def dumps(data):
        return yaml.safe_dump(data)

    @staticmethod
    def loads(text):
        return yaml.safe_load(text)

class Text(object):

    @staticmethod
    def dumps(data):
        text = ''
        for service in data.get('services', [data.get('service', data)]):
            for k, v in service.iteritems():
                text += '{0} {1}\n'.format(k, v)

            text += '\n'

        return text

    @staticmethod
    def loads(text):
        def extract_key_value_pair(line):
            if '=' in line:
                delim = '='
            else:
                delim = ' '

            key   = line.split(delim)[0]
            value = line[len(key) + 1:]
            key   = key.strip()
            value = value.strip().strip('"')
            return key, value

        def parse_numeric_value(value):
            try:
                if '.' in value:
                    value = float(value)
                else:
                    value = int(value)
            except ValueError:
                pass

            return value

        services = []
        service  = {}

        for line in text.split('\n'):
            line = line.strip()
            if not line:
                services.append(service)
                service = {}
                continue

            key, value   = extract_key_value_pair(line)
            service[key] = parse_numeric_value(value)

        if service:
            services.append(service)

        return {'services': services}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
