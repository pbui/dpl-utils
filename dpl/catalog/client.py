''' DPL Catalog - Client '''

from dpl.catalog import CATALOG_HOST, CATALOG_PORT, CATALOG_TYPE
from dpl.catalog.parsers import JSON

import dpl.util

import os

import tornado.httpclient
import tornado.ioloop
import tornado.web

# Catalog Client

class CatalogClient(object):

    def __init__(self, catalog_host=None, catalog_port=None):
        self.catalog_host = catalog_host or CATALOG_HOST
        self.catalog_port = catalog_port or CATALOG_PORT
        self.http_client  = tornado.httpclient.HTTPClient()

    def query(self):
        ''' Query Catalog Server

        Returns dict containing services information decoded from Catalog JSON.
        '''
        url      = 'http://{0}:{1}/services.json'.format(self.catalog_host, self.catalog_port)
        response = self.http_client.fetch(url)
        return JSON.loads(response.body)

    def status(self):
        ''' Print Catalog status

        Prints out table of Catalog services.
        '''
        # TODO: Make this customizable
        data           = self.query()
        services       = sorted(data['services'], key=lambda s: s.get('type', '') + s.get('name', ''))
        table_keys     = ['type', 'shortname', 'address', 'owner',
                          lambda s: dpl.util.timediff_as_string(s['lastheardfrom']) + ' ago']
        table_template = dpl.util.table_template(services, table_keys)

        print(table_template.format('Type', 'Name', 'Address', 'Owner', 'LastHeardFrom'))
        for service in services:
            print(table_template.format(*dpl.util.row_values(service, table_keys)))

    def update(self, **kwargs):
        ''' Send update to Catalog server

        Sends service record to Catalog server.  In addition to specified
        fields, it will include basic system information.
        '''
        url     = 'http://{0}:{1}/service/.json'.format(self.catalog_host, self.catalog_port)
        uname   = os.uname()
        load    = os.getloadavg()
        service = {
            'type'          : kwargs.get('type', CATALOG_TYPE),
            'name'          : os.environ.get('HOSTNAME', uname[1]),
            'opsys'         : uname[0],
            'opsysversion'  : uname[2],
            'cpu'           : uname[4],
            'load1'         : load[0],
            'load5'         : load[1],
            'load15'        : load[2],
            'owner'         : dpl.util.current_user(),
            'uptime'        : dpl.util.uptime(),
        }

        service.update(kwargs)
        data = {'services': [service]}

        self.http_client.fetch(url, method='POST', body=JSON.dumps(data))

# vim: sts=4 sw=4 ts=8 expandtab ft=python
