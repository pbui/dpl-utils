''' DPL Plugins - Processes '''

PROCESS_FIELDS = [
    'pid',
    'username',
    'cpu_percent',
    'memory_percent',
    'memory_info',
    'cmdline',
]

def plugin_update():
    processes = []

    try:
        import psutil

        _processes = []
        for process in psutil.process_iter():
            if process.cmdline():
                process.cpu_percent()
                _processes.append(process)

        for process in _processes:
            try:
                processes.append(process.as_dict(PROCESS_FIELDS))
            except psutil.NoSuchProcess:
                pass
    except ImportError:
        # TODO: implement fallback
        pass

    return {'processes': processes}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
