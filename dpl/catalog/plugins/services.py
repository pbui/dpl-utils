''' DPL Catalog Plugins - Services '''

import subprocess

def plugin_update():
    try:
        data = subprocess.check_output('/sbin/service --status-all 2> /dev/null', shell=True)
    except (subprocess.CalledProcessError, OSError):
        return {}

    services = {}
    for line in data.decode('utf-8').splitlines():
        if not line:
            continue

        name = line.split()[0]
        if 'running' in line:
            value = 'running'
        elif 'stopped' in line:
            value = 'stopped'
        else:
            continue

        services[name] = value

    return {'services': services}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
