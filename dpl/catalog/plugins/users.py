''' DPL Plugins - Users '''

import collections
import subprocess

def plugin_update():
    users = collections.defaultdict(list)

    try:
        import psutil
        for user in psutil.users():
            users[user.name].append({
                'terminal' : user.terminal,
                'host'     : user.host,
                'started'  : user.started,
            })
    except ImportError:
        try:
            data = subprocess.check_output('who')
        except subprocess.CalledProcessError:
            data = []

        for line in data.decode('utf-8').splitlines():
            user = line.split()
            users[user[0]].append({
                'terminal' : user[1],
                'host'     : user[4][1:-1],
                'started'  : ' '.join(user[2:4]),
            })

    return {'users': users}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
