''' DPL Catalog Plugins - cpu '''

def plugin_update():
    try:
        import psutil

        cpu_times = psutil.cpu_times()
        record    = {
            'cpu_count' : {'physical' : psutil.cpu_count(logical=False) or 1,
                           'logical'  : psutil.cpu_count(logical=True),
                          },
            'cpu_times' : {'user'     : cpu_times.user,
                           'system'   : cpu_times.system,
                           'idle'     : cpu_times.idle,
                           'nice'     : cpu_times.nice,
                          },
        }
    except ImportError:
        # TODO implement fallback
        record    = {}

    return record

# vim: sts=4 sw=4 ts=8 expandtab ft=python
