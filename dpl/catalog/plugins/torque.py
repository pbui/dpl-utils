''' DPL Plugins - Torque '''

import collections
import subprocess

def _qstat_time_as_seconds(qstat_time):
    hours, minutes, seconds = map(int, qstat_time.split(':'))
    return seconds + 60*minutes + 60*60*hours

def _torque_jobs():
    try:
        data = subprocess.check_output('qstat -f -n -1', shell=True)
    except subprocess.CalledProcessError:
        data = []

    jobs = []
    for line in data.splitlines():
        job = line.split()

        if not job or len(job) != 12 or line.startswith('Job'):
            continue

        jobs.append({
            'id'             : job[0],
            'owner'          : job[1],
            'queue'          : job[2],
            'name'           : job[3],
            'session_id'     : int(job[4] if job[4] != '--' else 0),
            'required_nodes' : int(job[5] if job[5] != '--' else 0),
            'required_tasks' : int(job[6] if job[6] != '--' else 0),
            'required_memory': int(job[7] if job[7] != '--' else 0),
            'required_time'  : _qstat_time_as_seconds(job[8] if job[8] != '--' else '0:0:0'),
            'status'         : job[9],
            'elapsed_time'   : _qstat_time_as_seconds(job[10] if job[10] != '--' else '0:0:0'),
            'nodes'          : list(set(job[11].split('+'))) if job[11] != '--' else [],
        })

    return jobs

def plugin_update():
    users = collections.defaultdict(list)
    
    # Only gather Torque information on the Torque server
    try:
        pbs_server = subprocess.check_output('pgrep ^pbs_server', shell=True)
        if not pbs_server:
            return {}
    except subprocess.CalledProcessError:
        return {}

    # TODO: also gather node information
    return {'torque': {'jobs': _torque_jobs()}}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
