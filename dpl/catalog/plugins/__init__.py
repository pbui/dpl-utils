''' DPL Catalog Plugins '''

def load_plugins(plugins):
    ''' Load each specified plugin '''
    record = {}

    for plugin in plugins:
        plugin_path = 'dpl.catalog.plugins.' + plugin
        module      = __import__(plugin_path, {}, {}, -1)
        record.update(module.plugin_update())

    return record

# vim: sts=4 sw=4 ts=8 expandtab ft=python
