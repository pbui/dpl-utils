''' DPL Catalog Plugins - psutil '''

def plugin_update():
    try:
        import psutil

        virtual_memory = psutil.virtual_memory()
        swap_memory    = psutil.swap_memory()

        record = {
            'virtual_memory' : {'total'    : virtual_memory.total,
                                'available': virtual_memory.available,
                                'used'     : virtual_memory.used,
                                'free'     : virtual_memory.free,
                                'cached'   : virtual_memory.cached,
                                'buffers'  : virtual_memory.buffers,
                                },
            'swap_memory'    : {'total'    : swap_memory.total,
                                'used'     : swap_memory.used,
                                'free'     : swap_memory.free,
                                'sin'      : swap_memory.sin,
                                'sout'     : swap_memory.sout,
                               },
        }
    except ImportError:
        # TODO: implement fallback
        record = {}

    return record

# vim: sts=4 sw=4 ts=8 expandtab ft=python
