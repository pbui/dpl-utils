''' DPL Catalog Plugins - psutil '''

ACCEPT_FSTYPES = ('ext2', 'ext3', 'ext4', 'xfs', 'nfs', 'jfs')

def plugin_update():
    try:
        import psutil

        def get_disk_partitions():
            disk_partitions = []
            for dp in psutil.disk_partitions(all=True):
                if dp.fstype not in ACCEPT_FSTYPES:
                    continue

                dp_usage = psutil.disk_usage(dp.mountpoint)
                disk_partitions.append({
                    'device'    : dp.device,
                    'mountpoint': dp.mountpoint,
                    'fstype'    : dp.fstype,
                    'opts'      : dp.opts,
                    'usage'     : { 'total': dp_usage.total,
                                    'used' : dp_usage.used,
                                    'free' : dp_usage.free,
                                  },
                })
            return disk_partitions

        record = {
            'disk_partitions'   : get_disk_partitions(),
        }
    except ImportError:
        # TODO: implement fallback
        record = {}

    return record

# vim: sts=4 sw=4 ts=8 expandtab ft=python
