''' DPL Catalog - Server'''

from dpl import __version__

from dpl.catalog import CATALOG_PORT, CATALOG_TITLE, CATALOG_COLLECT_TIMER, CATALOG_CHECKPOINT_TIMER
from dpl.catalog.database import CatalogDatabase
from dpl.catalog.handlers import CatalogHandler, ServiceHandler
from dpl.catalog.parsers import Text

import logging
import os
import socket

import tornado.httpclient
import tornado.ioloop
import tornado.options
import tornado.web


class CatalogHTTPServer(tornado.web.Application):
    def initialize(self, **settings):
        settings.update({
            'debug'         : True,
            'template_path' : os.path.join(os.path.dirname(__file__), 'templates'),
        })
        tornado.web.Application.__init__(self, **settings)

        self.add_handlers('.*', [
            (r'.*/'                     , CatalogHandler),
            # RESTful API
            (r'.*/services([.a-z]*)'    , CatalogHandler),
            (r'.*/service/([\w:.-]+)'   , ServiceHandler),
            # CCTools Catalog API Compatability
            (r'.*/query([.a-z]*)'       , CatalogHandler),
            (r'.*/detail/([\w:.-]+)'    , ServiceHandler),
        ])
        self.listen(self.port, xheaders=True) # Must set xheaders manually


class CatalogUDPServer(object):
    def initialize(self, **settings):
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket.bind(('0.0.0.0', self.port))

        self.ioloop.add_handler(self.udp_socket.fileno(), self.handle_datagram, self.ioloop.READ)

    def handle_datagram(self, fd, events):
        text, address = self.udp_socket.recvfrom(4096)
        data = Text.loads(text)

        self.database.record(data, address[0])


class CatalogServer(CatalogHTTPServer, CatalogUDPServer):
    def __init__(self, port=None, title=None, **settings):
        self.port     = port  or CATALOG_PORT
        self.title    = title or CATALOG_TITLE
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.database = CatalogDatabase(settings.get('db_path', None))
        self.version  = __version__

        tornado.options.parse_command_line()

        CatalogHTTPServer.initialize(self, **settings)
        CatalogUDPServer.initialize(self, **settings)

    def add_timers(self, timers):
        for callback, interval in timers:
            timer = tornado.ioloop.PeriodicCallback(callback, interval, io_loop=self.ioloop)
            timer.start()

    def run(self):
        self.add_timers([
            (self.database.collect,    CATALOG_COLLECT_TIMER),
            (self.database.checkpoint, CATALOG_CHECKPOINT_TIMER),
        ])

        self.ioloop.start()

# vim: sts=4 sw=4 ts=8 expandtab ft=python
