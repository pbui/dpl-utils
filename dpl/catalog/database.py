''' DPL Catalog - Database '''

from . import CATALOG_DB_PATH, CATALOG_TYPE, CATALOG_LIFETIME

import json
import logging
import os
import socket
import time


class CatalogDatabase(dict):
    def __init__(self, db_path=None, **kwargs):
        self.db_path = db_path or CATALOG_DB_PATH
        self.logger  = logging.getLogger()

        if os.path.exists(self.db_path):
            self.update(json.load(open(self.db_path)))
        else:
            self.update({'services': {}})

        db_root = os.path.dirname(self.db_path) or '.'
        if not os.path.exists(db_root):
            os.makedirs(db_root)

    @property
    def services(self):
        return self['services']

    @staticmethod
    def make_service_id(service):
        address = service.get('address', '')
        port    = service.get('port', 0)
        name    = service.get('shortname', service.get('name', address))
        return  '{0}:{1}:{2}'.format(address, port, name)

    def record(self, data, address=None):
        for service in data.get('services', []):
            if address:
                service['address'] = address

            if not 'type' in service:
                service['type'] = CATALOG_TYPE

            if not 'name' in service:
                # TODO: This is a blocking call, needs to be replaced with
                # something that is asynchronous.
                try:
                    service['name'] = socket.gethostbyaddr(service['address'])[0]
                except socket.herror:
                    service['name'] = service['address']

            service['shortname'] = service['name'].split('.')[0]
            service['lastheardfrom'] = time.time()

            service_id    = CatalogDatabase.make_service_id(service)
            service['id'] = service_id
            self.services[service_id] = service

            self.logger.info('Recorded: {0}'.format(service))

    def checkpoint(self):
        json.dump(self, open(self.db_path, 'w+'))

    def collect(self, lifetime=CATALOG_LIFETIME):
        for key in self.services.keys():
            service  = self.services[key]
            lifetime = min(lifetime, service.get('lifetime', lifetime))

            if service['lastheardfrom'] + lifetime < time.time():
                del self.services[key]
                self.logger.info('Collected: {0}'.format(service))

# vim: sts=4 sw=4 ts=8 expandtab ft=python
