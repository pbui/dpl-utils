''' DPL Catalog - Handlers '''

from dpl.catalog import CATALOG_SERVICE_TABLE
from dpl.catalog.parsers import JSON, YAML, Text

import dpl.util

import os
import time

import tornado.httpclient
import tornado.ioloop
import tornado.web


class BaseHandler(tornado.web.RequestHandler):
    GetFormatDispatch = {
        '.html': lambda s, d: s.get_html(d),
        '.json': lambda s, d: s.get_json(d),
        '.text': lambda s, d: s.get_text(d),
        '.yaml': lambda s, d: s.get_yaml(d),
    }
    PostFormatDispatch = {
        '.json': lambda s, d: s.post_json(d),
        '.text': lambda s, d: s.post_text(d),
    }


class CatalogHandler(BaseHandler):
    # TODO: support loading this from configuration file
    CatalogServiceTable = CATALOG_SERVICE_TABLE

    @staticmethod
    def make_catalog_url(params, **kwargs):
        p = {}
        p.update(params)
        p.update(kwargs)
        return 'services?' + '&'.join(['{0}={1}'.format(k, v) for k, v in p.items()])

    def get(self, format=None):
        format        = format or '.html'
        services      = self.application.database.services.values()
        display_type  = self.get_argument('display_type', 'all')
        sort_by       = self.get_argument('sort_by', 'name')
        sort_order    = self.get_argument('sort_order', 'asc')

        if display_type != 'all':
            services = [s for s in services if s['type'] == display_type]

        if sort_by:
            services = sorted(
                services, key=lambda s: str(s.get(sort_by, '')) + s.get('name', ''), reverse=sort_order == 'desc'
            )

        data = {
            'services'  : services,
            'timestamp' : time.time(),
            'params'    : {
                'display_type'  : display_type,
                'sort_by'       : sort_by,
                'sort_order'    : sort_order,
            },
        }
        CatalogHandler.GetFormatDispatch[format](self, data)

    def get_html(self, data):
        all_services          = self.application.database.services.values()
        service_types         = ['all'] + sorted(set(s['type'] for s in all_services))
        catalog_service_table = {}
        catalog_service_table.update(CatalogHandler.CatalogServiceTable)

        data.update({
            'catalog_service_table' : catalog_service_table,
            'catalog'               : self.application,
            'service_types'         : service_types,
            'make_catalog_url'      : CatalogHandler.make_catalog_url,
        })
        self.render('catalog.tmpl', **data)

    def get_json(self, data):
        self.write(JSON.dumps(data))

    def get_text(self, data):
        self.set_header('Content-Type', 'text/plain')
        self.write(Text.dumps(data))

    def get_yaml(self, data):
        self.set_header('Content-Type', 'text/plain')
        self.write(YAML.dumps(data))

class ServiceHandler(BaseHandler):
    def get(self, service_id=None):
        format = os.path.splitext(self.request.path)[-1]
        if format not in ServiceHandler.GetFormatDispatch.keys():
            format = '.html'
        else:
            service_id = service_id[:-len(format)] # Strip format extension

        # TODO: handle if service_id not in database
        data = {
            'service'  : self.application.database.services[service_id],
            'timestamp': time.time(),
        }

        ServiceHandler.GetFormatDispatch[format](self, data)

    def get_html(self, data):
        data.update({
            'dpl'     : dpl,
            'catalog' : self.application,
        })
        self.render('service.tmpl', **data)

    def get_json(self, data):
        self.write(JSON.dumps(data))

    def get_text(self, data):
        self.set_header('Content-Type', 'text/plain')
        self.write(Text.dumps(data))

    def get_yaml(self, data):
        self.set_header('Content-Type', 'text/plain')
        self.write(YAML.dumps(data))

    def post(self, service_id, format=None):
        format = '.' + self.request.path.split('.')[-1]
        if format not in ServiceHandler.PostFormatDispatch.keys():
            format = '.json'

        data = self.request.body
        ServiceHandler.PostFormatDispatch[format](self, data)

    def post_json(self, data):
        data = JSON.loads(self.request.body.decode('utf-8'))
        self.application.database.record(data, self.request.remote_ip)

    def post_text(self, data):
        data = Text.loads(data.decode('utf-8'))
        self.application.database.record(data, self.request.remote_ip)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
