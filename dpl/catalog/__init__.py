''' DPL Catalog

A RESTful Catalog server based on Tornado that is backwards compatible with the
original CCTools Catalog.
'''

import os

import dpl.util

CATALOG_HOST             = os.environ.get('CATALOG_HOST', 'dpl.cs.uwec.edu')
CATALOG_PORT             = int(os.environ.get('CATALOG_PORT', '9097'))
CATALOG_TITLE            = 'DPL Catalog'
CATALOG_DB_PATH          = 'catalog.db'
CATALOG_TYPE             = 'unknown'
CATALOG_LIFETIME         = 5*60
CATALOG_COLLECT_TIMER    = 5*60*1000
CATALOG_CHECKPOINT_TIMER = 5*60*1000
CATALOG_SERVICE_TABLE    = {
    'all': [
        ('Name'          , lambda s: s.get('shortname', s['name'])),
        ('Type'          , lambda s: s['type']),
        ('Address'       , lambda s: s['address']),
        ('Users'         , lambda s: s.get('nusers', len(s.get('users', {}).keys()))),
        ('Load1'         , lambda s: s['load1']),
        ('Load5'         , lambda s: s['load5']),
        ('Load15'        , lambda s: s['load15']),
        ('LastHeardFrom' , lambda s: dpl.util.timediff_as_string(s['lastheardfrom']) + ' ago'),
    ],
    'unix': [
        ('Name'          , lambda s: s.get('shortname', s['name'])),
        ('Address'       , lambda s: s['address']),
        ('CPU'           , lambda s: s['cpu']),
        ('Owner'         , lambda s: s['owner']),
        ('LastHeardFrom' , lambda s: dpl.util.timediff_as_string(s['lastheardfrom']) + ' ago'),
    ],
    'dpl': [
        ('Name'          , lambda s: s.get('shortname', s['name'])),
        ('Users'         , lambda s: s.get('nusers', len(s.get('users', {}).keys()))),
        ('Load1'         , lambda s: s['load1']),
        ('Load5'         , lambda s: s['load5']),
        ('Load15'        , lambda s: s['load15']),
        ('Uptime'        , lambda s: dpl.util.timespan_as_string(s['uptime'])),
        ('LastHeardFrom' , lambda s: dpl.util.timediff_as_string(s['lastheardfrom']) + ' ago'),
    ],
    'wq_master': [
        ('Name'          , lambda s: s.get('shortname', s['name'])),
        ('Project'       , lambda s: s['project']),
        ('Workers Init'  , lambda s: s['workers_init']),
        ('Workers Ready' , lambda s: s['workers_ready']),
        ('Workers Busy'  , lambda s: s['workers_busy']),
        ('Tasks Waiting' , lambda s: s['tasks_waiting']),
        ('Tasks Running' , lambda s: s['tasks_running']),
        ('Tasks Complete', lambda s: s['tasks_complete']),
        ('LastHeardFrom' , lambda s: dpl.util.timediff_as_string(s['lastheardfrom']) + ' ago'),
    ],
}

# vim: sts=4 sw=4 ts=8 expandtab ft=python
