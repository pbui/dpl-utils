''' DPL Utilities '''

import datetime
import itertools
import os
import platform
import pwd
import subprocess
import time

# Byte utilites

BYTES_TABLE = [
    (1 << 50, 'PB'),
    (1 << 40, 'TB'),
    (1 << 30, 'GB'),
    (1 << 20, 'MB'),
    (1 << 10, 'KB'),
]

def humanize_bytes(bytes):
    for value, suffix in BYTES_TABLE:
        if bytes >= value:
            return '{:.2f} {}'.format(float(bytes) / value, suffix)

    return '{} bytes'.format(bytes)

# List utilities

def split_zip_list(l):
    midpoint = len(l)//2
    if midpoint*2 < len(l):
        midpoint += 1
    return itertools.zip_longest(l[:midpoint], l[midpoint:], fillvalue=(u'', u''))

# Options utilities

def split_pair(s, delimiter):
    if delimiter in s:
        key   = s.split(delimiter)[0]
        value = s[len(key) + len(delimiter):]
        if value.isdigit():
            value = int(value)
    else:
        key   = s
        value = None
    return key, value

def parse_option_pairs(options):
    records = {}
    for option in options:
        key, value   = split_pair(option, '=')
        records[key] = value
    return records

def parse_host(host):
    if host:
        hostname, port = split_pair(host, ':')
    else:
        hostname, port = None, None
    return hostname, port

# Table utilities

def table_column_widths(table, keys):
    widths = [0]*len(keys)

    for row in table:
        for index, value in enumerate(row_values(row, keys)):
            key_width = len(value) + 1

            if widths[index] < key_width:
                widths[index] = key_width

    return widths

def table_template(table, keys):
    widths = table_column_widths(table, keys)
    return ' '.join(['{{{0}:{1}}}'.format(i, w) for i, w in enumerate(widths)])

def row_values(row, keys):
    values = []
    for key in keys:
        if callable(key):
            values.append(key(row))
        else:
            values.append(row[key])
    return values

# Time utilities

def timespan_as_string(time_span):
    time_unit  = 'second'
    time_pairs = [
        (60, 'minute'),
        (60, 'hour'),
        (24, 'day'),
    ]

    for limit, unit in time_pairs:
        if time_span < limit:
            break

        time_span = time_span / limit
        time_unit = unit

    time_span = int(round(time_span))
    if time_span != 1:
        time_unit = time_unit + 's'

    return '{0} {1}'.format(time_span, time_unit)

def timestamp_as_string(timestamp):
    if isinstance(timestamp, str):
        return timestamp

    return time.ctime(int(timestamp))

def timediff_as_string(start, end=None):
    if end is None:
        end = time.time()
    time_diff = end - start
    return timespan_as_string(time_diff)

# Uptime utilities

def uptime_linux():
    return float(open('/proc/uptime').read().split()[0])

def uptime_openbsd():
    try:
        data = subprocess.check_output(['sysctl', 'kern.boottime']).strip()
    except subprocess.CalledProcessError:
        return None

    try:
        uptime_string   = data.split('=')[-1]
        uptime_datetime = datetime.datetime.strptime(uptime_string, '%a %b %d %H:%M:%S %Y')
    except ValueError as e:
        return None

    uptime_delta = datetime.datetime.now() - uptime_datetime
    return float(uptime_delta.total_seconds())

_UptimeFunctions = {
    'Linux':    uptime_linux,
    'OpenBSD':  uptime_openbsd,
}

def uptime():
    try:
        uptime = _UptimeFunctions[platform.system()]()
    except KeyError:
        uptime = None

    return uptime

# User utilities

def current_user():
    return os.environ.get('USER', pwd.getpwuid(os.getuid()).pw_name)

# vim: sts=4 sw=4 ts=8 expandtab ft=python
